﻿using System;

namespace Squadra.Labs.GitLabCi.Example.Services.Abstractions
{
    public interface IExampleService
    {
        string Greet(string name);
    }
}
