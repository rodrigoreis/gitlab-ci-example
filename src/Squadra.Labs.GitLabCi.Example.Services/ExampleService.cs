﻿using Microsoft.Extensions.Configuration;
using Squadra.Labs.GitLabCi.Example.Services.Abstractions;

namespace Squadra.Labs.GitLabCi.Example.Services
{
    public class ExampleService : IExampleService
    {
        private readonly string _greetings;
        
        public ExampleService(IConfiguration configuration)
        {
            _greetings = configuration.GetValue<string>("Greetings");
        }

        public string Greet(string name) => string.Format(_greetings, name);
    }
}
