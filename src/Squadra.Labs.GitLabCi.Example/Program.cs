﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Squadra.Labs.GitLabCi.Example
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, builder) =>
                {
                    builder
                        .AddJsonFile(
                            path: "appsettings.json",
                            optional: false,
                            reloadOnChange: true
                        )
                        .AddJsonFile(
                            path: $"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json",
                            optional: true,
                            reloadOnChange: true
                        );
                })
                .UseUrls("http://+:5000", "https://+:5001")
                .UseStartup<Startup>();
    }
}
