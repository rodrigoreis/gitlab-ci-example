using System.Globalization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Squadra.Labs.GitLabCi.Example.Services;
using Squadra.Labs.GitLabCi.Example.Services.Abstractions;

namespace Squadra.Labs.GitLabCi.Example
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.TryAddScoped<IExampleService, ExampleService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.Run(async (context) =>
            {
                var exampleService = app.ApplicationServices.GetService<IExampleService>();
                
                await context.Response.WriteAsync(
                    text: exampleService.Greet("Lavras")
                );
            });
        }
    }
}
