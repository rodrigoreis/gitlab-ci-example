using System.IO;
using Microsoft.Extensions.Configuration;
using Squadra.Labs.GitLabCi.Example.Services;
using Xunit;

namespace Squadra.Labs.GitLabCi.Example.UnitTests
{
    public class ExampleUnitTest
    {
        private readonly IConfiguration _configuration;

        public ExampleUnitTest()
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();
        }
        
        [Theory]
        [InlineData("Lavras", "Hello Lavras!")]
        public void Test1(string name, string expected)
        {
            var service = new ExampleService(_configuration);
            Assert.Equal(expected, service.Greet(name));
        }
    }
}
